import firebase from "firebase";
import 'firebase/firestore';

// Initialize Firebase
const config = {
  apiKey: "AIzaSyBdndBli3vKxCN9mKgY9NhTgNREECp7vLA",
  authDomain: "quoly-1b134.firebaseapp.com",
  databaseURL: "https://quoly-1b134.firebaseio.com",
  projectId: "quoly-1b134",
  storageBucket: "quoly-1b134.appspot.com",
  messagingSenderId: "1009206745068",
};

firebase.initializeApp(config);

export default firebase;
