import { firebaseAction } from 'vuexfire';
import db from '@/db';

const state = {
  topics: [],
};

const actions = {
  init: firebaseAction(({ bindFirebaseRef }) => {
    bindFirebaseRef('topics', db.collection('topics'));
  }),
};

export default {
  namespaced: true,
  state,
  actions,
};
